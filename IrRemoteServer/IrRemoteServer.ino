// 2017/6/19 Based on IRremoteESP8266: IRServer (c) 2015 Mark Szabo (LPGL2.1)
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <IRsend.h>


const char* ssid = "YOUR_WIFI_SSID";
const char* password = "YOUR_WIFI_PASSWORD";

//IPAddress ip(192, 168, 10, 100); 
//IPAddress gateway(192, 168, 10, 1);
//IPAddress subnet(255, 255, 255, 0);

const char* hostName = "irrs999";  // ローカルネットで重複しないように要変更

const int ledPin = 13;
const int irLedPin = 5;
const int khz = 38;

ESP8266WebServer server(80);
IRsend irsend(irLedPin);

void handleRoot() {
  digitalWrite(ledPin, HIGH);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(ledPin, LOW);
}

void handleNotFound(){
  digitalWrite(ledPin, HIGH);
  String message = "Page Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(ledPin, LOW);
}

void handleIrSend(){
  digitalWrite(ledPin, HIGH);
  String message = "handleIrSend\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  String type = server.arg("type");
  String data = server.arg("data");
  type.toUpperCase();
  Serial.print(type + ":");
  if(type == "NEC") {
    String dataSize = server.arg("size");
    uint32_t irdata = strtoul(data.c_str(),NULL,16);
    Serial.println(irdata,HEX);
    irsend.sendNEC(irdata,dataSize.toInt());
  }else if(type == "PANASONIC") {
    String addr = server.arg("addr");
    uint16_t iraddr = strtoul(addr.c_str(),NULL,16);
    uint32_t irdata = strtoul(data.c_str(),NULL,16);
    Serial.print(iraddr,HEX);
    Serial.print(":");
    Serial.println(irdata,HEX);
    irsend.sendPanasonic(iraddr,irdata);
  }else if(type == "RAW") {
    String dataSize = server.arg("size");
    if(dataSize.toInt() <= 256){
      uint16_t rawData[dataSize.toInt()];
      char buf[10];
      for(int i = 0, ix = 0; i < data.length() && ix < sizeof(rawData); ix++, i++){
        buf[0] = '\0';
        for(int j = 0; j < sizeof(buf); j ++, i++){
          if(data.charAt(i) == ',' || i == data.length()){
            buf[j] = '\0' ;
            rawData[ix] = atoi(buf);
            Serial.print(rawData[ix]);
            Serial.print(":");
            break;
          } else{
            buf[j] = data.charAt(i);
          }
        }
      }
      Serial.println();
      Serial.print("RAW SIZE:");
      Serial.println(sizeof(rawData) / sizeof(rawData[0]));
      irsend.sendRaw(rawData, sizeof(rawData) / sizeof(rawData[0]), khz);
    }
  }
  server.send(200, "text/plain", message);
  digitalWrite(ledPin, LOW);
}

void setup(void){
  WiFi.mode(WIFI_STA);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  Serial.begin(115200);
  irsend.begin();
  //WiFi.config(ip, gateway, subnet);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(ledPin,HIGH);
    delay(250);
    digitalWrite(ledPin,LOW);
    delay(250);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin(hostName)) {
    Serial.println("MDNS responder started");
    MDNS.addService("http", "tcp", 80);
  }

  server.on("/", handleRoot);
  server.on("/irsend", handleIrSend);
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}