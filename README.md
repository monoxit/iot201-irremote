# 「リモコンサーバー」サンプルスケッチ #

MONOxITの「リモコンサーバー」で使用のサンプルスケッチです。

## IRremoteESP8266 ライブラリのバージョンが2.3以上のとき　##
受信コード解析に、このサイトに掲載のサンプルスケッチ ESP8266_IRrecvDumpV2　を使わないでください。
Arduino IDEのメニューから「スケッチ例」の「IRremoteESP8266」のIRrecvDumpV2 を開き、#define RECV_PIN を、受信モジュールを接続したピン番号を指定するように変更して使用してください。

## 使い方 ##

Arduino IDEのスケッチブックの保存場所にサンプルスケッチをコピーして使います。  
このbitbucketから、サンプルスケッチが含まれるZIP圧縮ファイルをダウンロードして、圧縮ファイル内のフォルダを、Arduino IDEのスケッチブックの保存場所にコピーします。

### コピーの手順 ###

* Webブラウザの画面を最大にする
* bitbucketのメニューから「ダウンロード」をクリック
* 「リポジトリをダウンロードする」をクリックしダウンロード
* ダウンロードフォルダに保存されたZIP圧縮ファイル内のmonoxit-iot201-irremote-0af434efad1cのような名前のフォルダ全体を、スケッチブックの保存場所にコピー  
※スケッチブックの保存場所はArduino IDEのメニューの「ファイル」の「環境設定」で表示される画面で確認できます。  
※フォルダ名の0af434efad1cの部分は異なる場合があります。

### サンプルスケッチの開き方 ###

Arduino IDEのメニューの「ファイル」の「スケッチブック」の「monoxit-iot201-irremote-0af434efad1c」からサンプルスケッチを開くことができます。

## LICENSE ##
サンプルスケッチはIRremoteESP8266のスケッチ例をベースにしています。
https://github.com/markszabo/IRremoteESP8266
https://github.com/markszabo/IRremoteESP8266/blob/master/LICENSE.txt