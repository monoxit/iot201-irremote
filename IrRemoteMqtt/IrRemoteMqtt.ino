/*
 * Basic MQTT IRremoteController
 *
 * Create credentials.h and put the bellow lines and change SSID, password and etc.
 * #define WIFI_SSID    "YOUR_WIFI_SSID"
 * #define WIFI_PASSWORD "YOUR_WIFI_PASSWORD"
 * #define MQTT_SERVER  "YOUR_MQTT_SERVER"
 * #define MQTT_SSL_PORT 8883
 * #define MQTT_USER "YOUR_MQTT_USER_ID"
 * #define MQTT_PASSWORD "API_TOKEN"
 * #define MQTT_FINGERPRINT "SHA1_FINGERPRING"
 *
 *  Copyright (c) 2018 Masami Yamakawa (MONOxIT Inc.)
 *   setupWifi(), reconnect() and parts of callback() functions are
 *     Copyright (c) 2008-2015 Nicholas O'Leary
 *  This software is released under the MIT License.
 *  http://opensource.org/licenses/mit-license.php
 */
#include "credentials.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <IRsend.h>

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;
const char* mqttServer = MQTT_SERVER;
const int   mqttPort = MQTT_SSL_PORT;
const char* mqttUser = MQTT_USER;
const char* mqttPassword = MQTT_PASSWORD;
const char* mqttFingerprint = MQTT_FINGERPRINT;

const int ledPin = 13;
const int irLedPin = 5;
const int khz = 38;

WiFiClientSecure espClient;
PubSubClient client(espClient);
IRsend irsend(irLedPin);

String mqttDeviceId = "ESP8266-";
String pubTopic = String(mqttUser) + "/f/irrecv";
String subTopic = String(mqttUser) + "/f/irsend";

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(115200);
  irsend.begin();
  setupWifi();
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
}

void setupWifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(ledPin,HIGH);
    delay(250);
    digitalWrite(ledPin,LOW);
    delay(250);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  mqttDeviceId += String(ESP.getChipId(),HEX);
}

void callback(char* topic, byte* payload, unsigned int length) {
  digitalWrite(ledPin, HIGH);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  // JSON形式で送られたメッセージの中身を取り出すためのバッファを準備
  StaticJsonBuffer<1024> jsonBuffer;
  // JSON形式の中身を取り出して、rootに入れる
  JsonObject& root = jsonBuffer.parseObject((char*) payload);
  String type = root["type"];
  String addr = root["addr"];
  int dataSize = root["size"];
  String data = root["data"];

  Serial.print("type: ");
  Serial.println(type);
  Serial.print("addr: ");
  Serial.println(addr);
  Serial.print("size: ");
  Serial.println(dataSize);
  Serial.print("data: ");
  Serial.println(data);

  type.toUpperCase();
  if(type == "NEC") {
    uint32_t irdata = strtoul(data.c_str(),NULL,16);
    Serial.println(irdata,HEX);
    irsend.sendNEC(irdata,dataSize);
  }else if(type == "PANASONIC") {
    uint16_t iraddr = strtoul(addr.c_str(),NULL,16);
    uint32_t irdata = strtoul(data.c_str(),NULL,16);
    Serial.print(iraddr,HEX);
    Serial.print(":");
    Serial.println(irdata,HEX);
    irsend.sendPanasonic(iraddr,irdata);
  }else if(type == "RAW") {
    if(dataSize <= 256){
      uint16_t rawData[dataSize];
      char buf[10];
      for(int i = 0, ix = 0; i < data.length() && ix < sizeof(rawData); ix++, i++){
        buf[0] = '\0';
        for(int j = 0; j < sizeof(buf); j ++, i++){
          if(data.charAt(i) == ',' || i == data.length()){
            buf[j] = '\0' ;
            rawData[ix] = atoi(buf);
            Serial.print(rawData[ix]);
            Serial.print(":");
            break;
          } else{
            buf[j] = data.charAt(i);
          }
        }
      }
      Serial.println();
      Serial.print("RAW SIZE:");
      Serial.println(sizeof(rawData) / sizeof(rawData[0]));
      irsend.sendRaw(rawData, sizeof(rawData) / sizeof(rawData[0]), khz);
    }
  }
  digitalWrite(ledPin, LOW);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection ");
    Serial.print(mqttDeviceId);
    Serial.print("...");
    // Attempt to connect
    if (client.connect(mqttDeviceId.c_str(), mqttUser, mqttPassword)) {
      if (espClient.verify(mqttFingerprint, mqttServer)) {
        Serial.println("connected");
        // Once connected, publish an announcement...
        client.publish(pubTopic.c_str(), mqttDeviceId.c_str());
        // ... and resubscribe
        client.subscribe(subTopic.c_str());
      }else {
        Serial.println("Bad certificate fingerprint! try again in 5 seconds");
        client.disconnect();
        for(int i = 0; i < 10; i++){
          digitalWrite(ledPin,HIGH);
          delay(500);
          digitalWrite(ledPin,LOW);
          delay(500);
        }
      }
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      for(int i = 0; i < 10; i++){
        digitalWrite(ledPin,HIGH);
        delay(500);
        digitalWrite(ledPin,LOW);
        delay(500);
      }
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
